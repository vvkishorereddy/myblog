import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import "./index.css";
import App from "./App";
// import Test from "./Test";

import store from "./Store";

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
      {/* <Test></Test> */}
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
