import React, { Component, Fragment } from "react";
import Axios from "axios";

class Test extends Component {
  state = {
    data: []
  };
  componentDidMount() {
    let i = 1;
    const test = () =>
      Axios.get(`http://localhost:5000/scrape?q=${i}`).then(response => {
        console.log(i);
        i++;
        if (i <= 100) {
          test();
        }
        let tempArr = [...this.state.data, ...response.data.body];
        tempArr.sort((a, b) => {
          var x = a.companyName.toLowerCase();
          var y = b.companyName.toLowerCase();
          if (x < y) {
            return -1;
          }
          if (x > y) {
            return 1;
          }
          return 0;
        });

        this.setState({
          data: tempArr
        });
      });
    test();
  }

  render() {
    return (
      <Fragment>
        <ol>
          {this.state.data &&
            this.state.data.map(row => <li key={row.id}>{row.companyName}</li>)}
        </ol>
      </Fragment>
    );
  }
}

export default Test;
