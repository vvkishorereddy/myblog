import { combineReducers } from "redux";
import { ArticlesReducer } from "../Components/Articles/reducer";
import { ArticleReducer } from "../Components/Article/reducer";
import { CommentsReducer } from "../Components/Comments/reducer";
import { newArticleReducer } from "../Components/ArticleCreate/reducer";
import { CommentCreateReducer } from "../Components/CommentCreate/reducer";
import { ArticleLikeReducer } from "../Components/Article/articleLikeReducer";

export default combineReducers({
  articles: ArticlesReducer,
  article: ArticleReducer,
  comments: CommentsReducer,
  createArticle: newArticleReducer,
  createComment: CommentCreateReducer,
  likeArticle: ArticleLikeReducer
});
