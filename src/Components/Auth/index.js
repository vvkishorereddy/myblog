import React, { Component, Fragment } from "react";
import {
  Grid,
  Paper,
  TextField,
  Button,
  Box,
  Divider
} from "@material-ui/core";
import Axios from "axios";

export default class Auth extends Component {
  state = {
    email: "",
    password: "",
    menuStatus: true
  };

  handleOnChange = event => {
    const { name, value } = event.currentTarget;
    this.setState({ [name]: value });
  };

  handleOnSubmit = () => {
    const accountType = this.state.menuStatus ? "login" : "signup";
    Axios.post("http://localhost:3005/users", {
      email: this.state.email,
      password: this.state.password,
      accountType: accountType
    }).then(data => {
      if (data.status === 201) {
        this.setState({ email: "", password: "" });
      }
    });
  };

  changeFormType = value => {
    this.setState({ menuStatus: value });
  };

  render() {
    const minHeight = window.innerHeight - 148;
    return (
      <Fragment>
        <Grid
          container
          justify="center"
          alignItems="center"
          style={{ minHeight: minHeight }}
        >
          <Grid item xs={6}>
            <Paper style={{ padding: 10 }}>
              <Box
                style={{
                  display: "flex",
                  justifyContent: "center",
                  marginBottom: 10
                }}
              >
                <Button
                  onClick={() => this.changeFormType(true)}
                  variant={this.state.menuStatus ? "contained" : "outlined"}
                  color="primary"
                  style={{
                    borderTopRightRadius: 0,
                    borderBottomRightRadius: 0
                  }}
                >
                  Sign In
                </Button>
                <Button
                  onClick={() => this.changeFormType(false)}
                  variant={this.state.menuStatus ? "outlined" : "contained"}
                  color="primary"
                  style={{ borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
                >
                  Sign Up
                </Button>
              </Box>

              <Divider />
              <form>
                <TextField
                  label="Email"
                  type="email"
                  fullWidth
                  onChange={this.handleOnChange}
                  name="email"
                  value={this.state.email}
                />
                <TextField
                  label="Password"
                  type="password"
                  fullWidth
                  onChange={this.handleOnChange}
                  name="password"
                  value={this.state.password}
                />

                <Box style={{ display: "flex", justifyContent: "flex-end" }}>
                  <Button
                    color="primary"
                    variant="outlined"
                    type="button"
                    style={{ marginTop: 5 }}
                    onClick={this.handleOnSubmit}
                  >
                    {this.state.menuStatus ? "Sign In" : "Create"}
                  </Button>
                </Box>
              </form>
            </Paper>
          </Grid>
        </Grid>
      </Fragment>
    );
  }
}
