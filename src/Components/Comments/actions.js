import {
  COMMENTS_REQUEST,
  COMMENTS_RECEIVED,
  COMMENTS_FAILED
} from "../../Store/types";
import Axios from "axios";

export const getComments = PostId => {
  return dispatch => {
    dispatch({ type: COMMENTS_REQUEST });
    Axios.get(`http://localhost:5000/articles/${PostId}/comments`)
      .then(response =>
        dispatch({ type: COMMENTS_RECEIVED, payload: response.data })
      )
      .catch(err => dispatch({ type: COMMENTS_FAILED, payload: err }));
  };
};
