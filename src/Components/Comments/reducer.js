import {
  COMMENTS_REQUEST,
  COMMENTS_RECEIVED,
  COMMENTS_FAILED
} from "../../Store/types";

const initialState = {
  isLoading: false,
  data: [],
  error: ""
};

export const CommentsReducer = (state = initialState, action) => {
  switch (action.type) {
    case COMMENTS_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case COMMENTS_RECEIVED:
      return {
        ...state,
        isLoading: false,
        data: action.payload
      };
    case COMMENTS_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };
    default:
      return state;
  }
};
