import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Grid, Card, CardContent, CardHeader, Box } from "@material-ui/core";
import CommentRow from "../CommentRow";
import CommentCreate from "../CommentCreate";
import { getComments } from "./actions";
class Comments extends Component {
  componentDidMount() {
    const { Id } = this.props.match.params;
    this.props.getComments(Id);
  }

  render() {
    const { data, isLoading, error } = this.props.comments;

    return (
      <Card style={{ marginTop: 10 }}>
        <CardHeader title="Comments" />
        <CardContent>
          <Grid
            container
            direction="column"
            spacing={1}
            style={{ width: "80%" }}
          >
            {data.map(comment => {
              return <CommentRow key={comment.id} {...comment} />;
            })}
            <CommentCreate postId={this.props.match.params.Id} />
          </Grid>
        </CardContent>
      </Card>
    );
  }
}

const mapStateToProps = ({ comments }) => ({ comments });

export default connect(
  mapStateToProps,
  { getComments }
)(withRouter(Comments));
