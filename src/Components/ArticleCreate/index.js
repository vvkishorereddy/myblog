import React, { Component, Fragment } from "react";
import {
  Grid,
  Paper,
  Typography,
  TextField,
  Button,
  Box,
  Divider,
  FormControl,
  Select,
  MenuItem,
  InputLabel
} from "@material-ui/core";

import { connect } from "react-redux";

import { createArticle } from "./actions";

const categoryList = [
  {
    id: "1",
    name: "Technology"
  },
  {
    id: "2",
    name: "Design"
  },
  {
    id: "3",
    name: "Culture"
  },
  {
    id: "4",
    name: "Business"
  },
  {
    id: "5",
    name: "Politics"
  },
  {
    id: "6",
    name: "Opinion"
  },
  {
    id: "7",
    name: "Science"
  },
  {
    id: "8",
    name: "Health"
  },
  {
    id: "9",
    name: "Style"
  },
  {
    id: "10",
    name: "Travel"
  }
];

export class ArticleCreate extends Component {
  state = {
    title: "",
    body: "",
    category: ""
  };

  handleOnChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleOnSubmit = () => {
    this.props.createArticle(this.state).then(data => {
      if (data.status === 201) {
        this.setState({ title: "", body: "" });
      }
    });
  };

  render() {
    const minHeight = window.innerHeight - 148;
    return (
      <Fragment>
        <Grid
          container
          justify="center"
          alignItems="center"
          style={{ minHeight: minHeight }}
        >
          <Grid item xs={6}>
            <Paper style={{ padding: 10 }}>
              <Typography variant="h5" align="center">
                New Article
              </Typography>
              <Divider />
              <form>
                <FormControl fullWidth>
                  <TextField
                    label="Title"
                    onChange={this.handleOnChange}
                    name="title"
                    value={this.state.title}
                  />
                </FormControl>
                <FormControl fullWidth>
                  <TextField
                    label="Description"
                    multiline
                    rowsMax={10}
                    onChange={this.handleOnChange}
                    name="body"
                    value={this.state.body}
                  />
                </FormControl>
                <FormControl fullWidth>
                  <InputLabel htmlFor="category">Category</InputLabel>
                  <Select
                    value={this.state.category}
                    name="category"
                    id="category"
                    onChange={this.handleOnChange}
                  >
                    {categoryList.map(category => (
                      <MenuItem
                        key={category.id}
                        value={category.name.toLowerCase()}
                      >
                        {category.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>

                <Box style={{ display: "flex", justifyContent: "flex-end" }}>
                  <Button
                    color="primary"
                    variant="outlined"
                    type="button"
                    style={{ marginTop: 5 }}
                    onClick={this.handleOnSubmit}
                  >
                    Save
                  </Button>
                </Box>
              </form>
            </Paper>
          </Grid>
        </Grid>
      </Fragment>
    );
  }
}

//const mapStateToProps=({})=>({})

export default connect(
  null,
  { createArticle }
)(ArticleCreate);
