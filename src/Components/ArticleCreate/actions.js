import Axios from "axios";

import {
  POST_CREATING,
  POST_CREATED,
  POST_CREATING_ERROR
} from "../../Store/types";

export const createArticle = postData => {
  return dispatch => {
    dispatch({ type: POST_CREATING });

    return new Promise((resolve, reject) => {
      Axios.post(`http://localhost:5000/articles`, { ...postData })
        .then(response => {
          dispatch({
            type: POST_CREATED,
            payload: { status: response.status, data: response.data }
          });
          resolve(response);
        })
        .catch(err => {
          dispatch({ type: POST_CREATING_ERROR, payload: err });
          reject(err);
        });
    });
  };
};
