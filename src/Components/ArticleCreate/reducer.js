import {
  POST_CREATING,
  POST_CREATED,
  POST_CREATING_ERROR
} from "../../Store/types";

const initilState = {
  isLoading: false,
  data: {},
  error: ""
};

export const newArticleReducer = (state = initilState, action) => {
  switch (action.type) {
    case POST_CREATING:
      return {
        ...state,
        isLoading: true
      };
    case POST_CREATED:
      return {
        ...state,
        isLoading: false,
        data: action.payload
      };
    case POST_CREATING_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };
    default:
      return state;
  }
};
