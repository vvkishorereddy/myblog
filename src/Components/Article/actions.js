import {
  POST_REQUEST,
  POST_RECEIVED,
  POST_FAILED,
  POST_LIKE_CREATE_FAILED,
  POST_LIKE_CREATE_RECEIVED,
  POST_LIKE_CREATE_REQUEST
} from "../../Store/types";
import Axios from "axios";

export const singlePost = id => {
  return dispatch => {
    dispatch({
      type: POST_REQUEST
    });
    Axios.get(`http://localhost:5000/articles/${id}`)
      .then(response =>
        dispatch({ type: POST_RECEIVED, payload: response.data })
      )
      .catch(err => dispatch({ type: POST_FAILED, payload: err }));
  };
};

export const likePost = id => dispatch => {
  dispatch({
    type: POST_LIKE_CREATE_REQUEST
  });
  Axios.post(`http://localhost:5000/articles/${id}/like`)
    .then(response =>
      dispatch({ type: POST_LIKE_CREATE_RECEIVED, payload: response.data })
    )
    .catch(err => dispatch({ type: POST_LIKE_CREATE_FAILED, payload: err }));
};
