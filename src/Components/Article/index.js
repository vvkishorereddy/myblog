import React, { Component, Fragment } from "react";

import { connect } from "react-redux";
import {
  Grid,
  Typography,
  Card,
  CardContent,
  CardHeader,
  Box,
  Icon,
  Divider
} from "@material-ui/core";
import Loader from "../Loader";
import HomeBanner from "../HomeBanner";
import Comments from "../Comments";

import { singlePost, likePost } from "./actions";

class Article extends Component {
  componentDidMount() {
    const { Id } = this.props.match.params;
    this.props.singlePost(Id);
  }

  handleClickLike = () => {
    const { Id } = this.props.match.params;
    this.props.likePost(Id);
  };

  render() {
    const {
      isLoading,
      data: { title, body, likes = 0, created_at = " 7th Sept 2019" }
    } = this.props.article;

    return isLoading ? (
      <Loader />
    ) : (
      <Fragment>
        <HomeBanner title={title} />
        <Grid container>
          <Grid item xs={12}>
            <Card>
              <CardHeader title={title} />
              <CardContent>
                {body}
                <Divider style={{ marginTop: 10, marginBottom: 10 }} />
                <Box
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center"
                  }}
                >
                  <Box
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Icon
                      fontSize="small"
                      style={{ color: "#4caf50", marginRight: 3 }}
                      onClick={this.handleClickLike}
                    >
                      thumb_up
                    </Icon>
                    <Typography
                      variant="caption"
                      style={{ color: "#4caf50", marginRight: 3 }}
                    >
                      {likes}
                    </Typography>
                  </Box>
                  <Box
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Icon
                      fontSize="small"
                      style={{ color: "#4caf50", marginRight: 3 }}
                    >
                      calendar_today
                    </Icon>
                    <Typography
                      variant="caption"
                      style={{ color: "#4caf50", marginRight: 3 }}
                    >
                      {created_at}
                    </Typography>
                  </Box>
                </Box>
              </CardContent>
            </Card>
            <Comments />
          </Grid>
        </Grid>
      </Fragment>
    );
  }
}

const mapStateToProps = ({ article, likeArticle }) => ({
  article,
  likeArticle
});

export default connect(
  mapStateToProps,
  { singlePost, likePost }
)(Article);
