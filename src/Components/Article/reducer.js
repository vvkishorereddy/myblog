import { POST_REQUEST, POST_RECEIVED, POST_FAILED } from "../../Store/types";

const initialState = {
  isLoading: false,
  data: {},
  error: ""
};

export const ArticleReducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case POST_RECEIVED:
      return {
        ...state,
        isLoading: false,
        data: action.payload
      };
    case POST_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };
    default:
      return state;
  }
};
