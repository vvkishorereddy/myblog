import {
  POST_LIKE_CREATE_FAILED,
  POST_LIKE_CREATE_RECEIVED,
  POST_LIKE_CREATE_REQUEST
} from "../../Store/types";

const initialState = {
  isLoading: false,
  data: {},
  error: ""
};

export const ArticleLikeReducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_LIKE_CREATE_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case POST_LIKE_CREATE_RECEIVED:
      return {
        ...state,
        isLoading: false,
        data: action.payload
      };
    case POST_LIKE_CREATE_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };
    default:
      return state;
  }
};
