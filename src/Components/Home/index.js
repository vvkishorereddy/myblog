import React, { Component, Fragment } from "react";
import { Grid } from "@material-ui/core";

import HomeBanner from "../HomeBanner";
import RightSection from "../RightSection";
import Articles from "../Articles";

export class Home extends Component {
  render() {
    return (
      <Fragment>
        <HomeBanner title="Latest Atricles" />
        <Grid spacing={2} container style={{ marginTop: "1px" }}>
          <Grid item xs={12} lg={9}>
            <Articles />
          </Grid>
          <Grid item xs={12} lg={3}>
            <RightSection />
          </Grid>
        </Grid>
      </Fragment>
    );
  }
}

export default Home;
