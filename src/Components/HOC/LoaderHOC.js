import React, { Component } from "react";

export default function LoaderHOC(WrappedComponent) {
  return class extends Component {
    render() {
      console.log(this.props, "hoc");
      return <WrappedComponent {...this.props} />;
    }
  };
}
