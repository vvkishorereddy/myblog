import React from "react";
import ChevronRight from "@material-ui/icons/ChevronRight";
import {
  Paper,
  Typography,
  List,
  ListItem,
  ListItemIcon,
  ListItemText
} from "@material-ui/core";

const categoryArray = [
  {
    id: "1",
    title: "Technology"
  },
  {
    id: "2",
    title: "Design"
  },
  {
    id: "3",
    title: "Culture"
  },
  {
    id: "4",
    title: "Business"
  },
  {
    id: "5",
    title: "Politics"
  },
  {
    id: "6",
    title: "Opinion"
  },
  {
    id: "7",
    title: "Science"
  },
  {
    id: "8",
    title: "Health"
  },
  {
    id: "9",
    title: "Style"
  },
  {
    id: "10",
    title: "Travel"
  }
];

function RightSection() {
  return (
    <Paper style={{ paddingLeft: "15px", paddingTop: "15px" }} elevation={0}>
      <Typography align="left" component="h5" variant="h6">
        Category
      </Typography>
      <List>
        {categoryArray.map(({ id, title }) => (
          <ListItem key={id}>
            <ListItemIcon>
              <ChevronRight />
            </ListItemIcon>
            <ListItemText primary={title} />
          </ListItem>
        ))}
      </List>
    </Paper>
  );
}

export default RightSection;
