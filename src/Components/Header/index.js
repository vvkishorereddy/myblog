import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { Toolbar, Typography, Button, Divider, Box } from "@material-ui/core";

function Header() {
  return (
    <Fragment>
      <Toolbar>
        <Box style={{ flex: 1 }} align="center">
          <Typography
            variant="h5"
            component={Link}
            color="inherit"
            to="/"
            style={{ textDecoration: "none" }}
          >
            My Blog
          </Typography>
        </Box>
        <Button size="small" variant="outlined" to="/auth" component={Link}>
          SignIn/SignUp
        </Button>
      </Toolbar>
      <Divider />
    </Fragment>
  );
}

export default Header;
