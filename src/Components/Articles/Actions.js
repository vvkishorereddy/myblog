import { POSTS_REQUEST, POSTS_RECEIVED, POSTS_FAILED } from "../../Store/types";
import Axios from "axios";

export const getPosts = () => {
  return dispatch => {
    dispatch({ type: POSTS_REQUEST });
    return Axios.get(`http://localhost:5000/articles`)
      .then(response =>
        dispatch({
          type: POSTS_RECEIVED,
          payload: response.data
        })
      )
      .catch(err => dispatch({ type: POSTS_FAILED, payload: err }));
  };
};
