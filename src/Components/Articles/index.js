import React, { Component } from "react";
import { connect } from "react-redux";

import { Grid } from "@material-ui/core";
import ArticleRow from "../ArticleRow";
import Loader from "../Loader";

import { getPosts } from "./actions";

class Articles extends Component {
  componentDidMount() {
    this.props.getPosts();
  }

  render() {
    const { articles } = this.props;

    return !articles.isLoading ? (
      <Grid spacing={2} container>
        {articles.data.map(post => {
          return <ArticleRow {...post} key={post.id} />;
        })}
      </Grid>
    ) : (
      <Loader />
    );
  }
}

const mapStateToProps = ({ articles }) => ({ articles });

export default connect(
  mapStateToProps,
  { getPosts }
)(Articles);
