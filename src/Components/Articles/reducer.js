import { POSTS_REQUEST, POSTS_RECEIVED, POSTS_FAILED } from "../../Store/types";
const initialState = {
  isLoading: false,
  data: [],
  error: ""
};

export const ArticlesReducer = (state = initialState, action) => {
  switch (action.type) {
    case POSTS_REQUEST:
      return { ...state, isLoading: true };
    case POSTS_RECEIVED:
      return { ...state, data: action.payload, isLoading: false };
    case POSTS_FAILED:
      return { ...state, error: action.payload, isLoading: false };
    default:
      return state;
  }
};
