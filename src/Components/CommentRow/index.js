import React from "react";

import { Grid, Typography, Card, CardContent } from "@material-ui/core";
function CommentRow({ body, email, name }) {
  return (
    <Grid item>
      <Card elevation={0} style={{ border: "1px solid #ccc" }}>
        <CardContent>
          <Typography variant="caption">
            <b>Name:</b> {name} &nbsp;
          </Typography>
          <Typography variant="caption">
            <b>Email:</b>
            {email}
          </Typography>
          <Typography variant="body2">{body}</Typography>
        </CardContent>
      </Card>
    </Grid>
  );
}

export default CommentRow;
