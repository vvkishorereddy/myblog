import React from "react";
import { Link } from "react-router-dom";
import { textLimit } from "../../Helpers";
import {
  Typography,
  Button,
  Grid,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  CardMedia,
  Icon,
  Box,
  Avatar
} from "@material-ui/core";

function ArticleRow({
  id,
  title,
  body,
  createdDate = "Posted on 7th Sept",
  likes = 45,
  comments = 130
}) {
  return (
    <Grid item xs={4}>
      <Card>
        <CardHeader
          avatar={<Avatar style={{ backgroundColor: "#4caf50" }}>25 </Avatar>}
          title={title && textLimit(title, 25)}
          subheader={createdDate}
        />
        <CardMedia
          component="img"
          height="140"
          image="https://via.placeholder.com/150"
        />
        <CardContent style={{ minHeight: 150, borderBottom: "1px solid #ccc" }}>
          <Typography variant="body2" color="textSecondary" component="p">
            {body && textLimit(body, 200)}
          </Typography>
        </CardContent>

        <CardActions
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between"
          }}
        >
          <Box
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Icon fontSize="small" style={{ color: "#4caf50", marginRight: 3 }}>
              thumb_up
            </Icon>
            <Typography
              variant="caption"
              style={{ color: "#4caf50", marginRight: 3 }}
            >
              {likes}
            </Typography>
          </Box>
          <Button
            size="small"
            style={{ color: "#4caf50" }}
            component={Link}
            to={`/article/${id}`}
          >
            Read More
          </Button>
        </CardActions>
      </Card>
    </Grid>
  );
}

export default ArticleRow;
