import React from "react";
import { CircularProgress } from "@material-ui/core";

function Loader() {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        minHeight: window.innerHeight + "px"
      }}
    >
      <CircularProgress color="secondary" />
    </div>
  );
}

export default Loader;
