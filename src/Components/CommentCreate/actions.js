import {
  COMMENT_CREATE_REQUEST,
  COMMENT_CREATE_RECEIVED,
  COMMENT_CREATE_FAILED
} from "../../Store/types";
import Axios from "axios";

export const postComment = commentData => {
  return dispatch => {
    dispatch({ type: COMMENT_CREATE_REQUEST });
    return new Promise((resolve, reject) => {
      const postId = commentData.postId;
      const name = commentData.name;
      const email = commentData.email;
      const body = commentData.body;
      Axios.post(`http://localhost:5000/comments`, {
        name,
        email,
        body,
        postId
      })
        .then(response => {
          dispatch({
            type: COMMENT_CREATE_RECEIVED,
            payload: { status: response.status, data: response.data }
          });
          resolve(response);
        })
        .catch(e => {
          dispatch({ type: COMMENT_CREATE_FAILED, payload: e });
          reject(e);
        });
    });
  };
};
