import React, { Component } from "react";
import { connect } from "react-redux";
import { postComment } from "./actions";
import {
  Paper,
  FormControl,
  Typography,
  Divider,
  TextField,
  Grid,
  Box,
  Button
} from "@material-ui/core";

class CommentCreate extends Component {
  state = {
    name: "",
    email: "",
    body: ""
  };

  handleOnChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleOnSubmit = () => {
    this.props
      .postComment({ ...this.state, postId: this.props.postId })
      .then(response => {
        if (response.status === 201) {
          this.setState({
            name: "",
            email: "",
            body: ""
          });
        }
      });
  };

  render() {
    return (
      <Paper>
        <Typography variant="h6" align="center">
          New Comment
        </Typography>
        <Divider />
        <form>
          <Grid container spacing={1}>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <TextField
                  label="Name"
                  onChange={this.handleOnChange}
                  name="name"
                  value={this.state.name}
                />
              </FormControl>
            </Grid>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <TextField
                  label="Email"
                  onChange={this.handleOnChange}
                  name="email"
                  value={this.state.email}
                />
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth>
                <TextField
                  label="Comment"
                  onChange={this.handleOnChange}
                  name="body"
                  multiline
                  rowsMax={10}
                  value={this.state.body}
                />
              </FormControl>
            </Grid>
            <Grid item>
              <Box style={{ display: "flex", justifyContent: "flex-end" }}>
                <Button
                  color="primary"
                  variant="outlined"
                  type="button"
                  style={{ margin: 5 }}
                  onClick={this.handleOnSubmit}
                >
                  Save
                </Button>
              </Box>
            </Grid>
          </Grid>
        </form>
      </Paper>
    );
  }
}

const mapStateToProps = ({ createComment }) => ({ createComment });

export default connect(
  mapStateToProps,
  { postComment }
)(CommentCreate);
