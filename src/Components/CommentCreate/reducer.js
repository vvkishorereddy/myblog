import {
  COMMENT_CREATE_REQUEST,
  COMMENT_CREATE_RECEIVED,
  COMMENT_CREATE_FAILED
} from "../../Store/types";

const initialState = {
  isLoading: false,
  data: [],
  error: ""
};

export const CommentCreateReducer = (state = initialState, action) => {
  switch (action.type) {
    case COMMENT_CREATE_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case COMMENT_CREATE_RECEIVED:
      return {
        ...state,
        isLoading: false,
        data: action.payload
      };
    case COMMENT_CREATE_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };
    default:
      return state;
  }
};
