import React from "react";
import { Container, Typography } from "@material-ui/core";

function Footer() {
  return (
    <footer style={{ backgroundColor: "gray", marginTop: "15px" }}>
      <Container maxWidth="lg">
        <Typography variant="h6" align="center" color="initial" gutterBottom>
          Footer
        </Typography>
        <Typography
          variant="subtitle1"
          align="center"
          color="textSecondary"
          component="p"
        >
          Something here to give the footer a purpose!
        </Typography>
      </Container>
    </footer>
  );
}

export default Footer;
