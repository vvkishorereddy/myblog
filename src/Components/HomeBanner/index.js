import React from "react";
import { Paper, Grid, Typography } from "@material-ui/core";

function HomeBanner({ title }) {
  return (
    <Paper
      style={{
        backgroundImage: "url(/images/header.jpg)",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
        height: "300px",
        position: "relative"
      }}
    >
      <div
        style={{
          position: "absolute",
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          backgroundColor: "rgba(0,0,0,0.2)"
        }}
      />
      <Grid container justify="center" alignItems="center" direction="row">
        <Grid item>
          <Typography
            variant="h3"
            component="h1"
            gutterBottom
            align="center"
            color="inherit"
            style={{
              position: "relative",
              color: "white",
              marginTop: "50px"
            }}
          >
            {title}
          </Typography>
        </Grid>
      </Grid>
    </Paper>
  );
}

export default HomeBanner;
