import React, { Component, Fragment, Suspense, lazy } from "react";
import { Switch, Route } from "react-router-dom";
import { CssBaseline, Container } from "@material-ui/core";

import Footer from "./Components/Footer";
import Header from "./Components/Header";
import Loader from "./Components/Loader";

const Home = lazy(() => import("./Components/Home"));
const Auth = lazy(() => import("./Components/Auth"));
const Error404 = lazy(() => import("./Components/NotFound/404"));
const Article = lazy(() => import("./Components/Article"));
const ArticleCreate = lazy(() => import("./Components/ArticleCreate"));

export class App extends Component {
  render() {
    return (
      <Fragment>
        <CssBaseline />
        <Container maxWidth="lg">
          <Header />
          <main>
            <Suspense fallback={<Loader />}>
              <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/auth" component={Auth} />
                <Route path="/article/create" exact component={ArticleCreate} />
                <Route path="/article/:Id" component={Article} />
                <Route component={Error404} />
              </Switch>
            </Suspense>
          </main>
        </Container>
        <Footer />
      </Fragment>
    );
  }
}

export default App;
