const DB = require("./DB");

class Article extends DB {
  createArticle(data) {
    const id = this.generateShortId();
    this.db
      .get("posts")
      .push({
        ...data,
        id: id,
        likes: 0,
        created_at: new Date().toLocaleString()
      })
      .write();

    return this.db
      .get("posts")
      .find({ id: id })
      .value();
  }
  getArticles() {
    return this.db.get("posts").value();
  }
  getArticleById(Id) {
    return this.db
      .get("posts")
      .find({ id: Id })
      .value();
  }
  updateLikes(postId) {
    const likes = this.db
      .get("posts")
      .find({ id: postId })
      .value().likes;

    return this.db
      .get("posts")
      .find({ id: postId })
      .assign({ likes: likes + 1 })
      .write();
  }
}

module.exports = Article;
