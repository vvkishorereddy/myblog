const shortId = require("shortid");
class DB {
  constructor() {
    const path = require("path");
    const low = require("lowdb");
    const FileSync = require("lowdb/adapters/FileSync");

    const adapter = new FileSync(path.join(__dirname, "db.json"));
    this.db = low(adapter);
    return this;
  }
  setDefault() {
    this.db.defaults({ posts: [], comments: [], users: [] }).write();
  }
  generateShortId() {
    shortId.characters(
      "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@"
    );

    return shortId.generate();
  }
}

module.exports = DB;
