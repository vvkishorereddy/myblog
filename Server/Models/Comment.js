const DB = require("./DB");

class Comment extends DB {
  getCommentsByPostId(postId) {
    return this.db
      .get("comments")
      .filter({ postId: postId })
      .value();
  }
  getAllComments() {
    return this.db
      .get("comments")

      .value();
  }
  createComment(data) {
    const id = this.generateShortId();
    this.db
      .get("comments")
      .push({ ...data, id: id, created_at: new Date().toLocaleString() })
      .write();

    return this.db
      .get("comments")
      .find({ id: id })
      .value();
  }
}

module.exports = Comment;
