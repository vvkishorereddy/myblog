const CommentModel = require("../Models/Comment");
const commentmodel = new CommentModel();

module.exports = {
  getCommentsByPostId: (req, res) => {
    const data = commentmodel.getCommentsByPostId(req.params.postId);
    res.json(data);
  },
  getAll: (req, res) => {
    const data = commentmodel.getAllComments();
    res.json(data);
  },
  createComment: (req, res) => {
    const data = commentmodel.createComment(req.body);
    res.json(data);
  }
};
