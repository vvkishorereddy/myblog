const Article = require("../Models/Article");
const article = new Article();

module.exports = {
  getAll: async (req, res) => {
    const data = article.getArticles();
    return res.json(data);
  },
  get: async (req, res) => {
    const data = article.getArticleById(req.params.id);
    return res.json(data);
  },
  create: (req, res) => {
    const data = article.createArticle(req.body);
    res.json(data);
  },
  likeArticle: (req, res) => {
    const data = article.updateLikes(req.params.id);
    res.json(data);
  }
};
