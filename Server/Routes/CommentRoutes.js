const express = require("express");
const router = express.Router();

const CommentController = require("../Controllers/CommentController");

router.get("/", CommentController.getAll);
router.post("/", CommentController.createComment);

module.exports = router;
