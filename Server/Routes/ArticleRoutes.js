const express = require("express");
const router = express.Router();
const ArticleController = require("../Controllers/ArticleController");
const CommentController = require("../Controllers/CommentController");

router.get("/", ArticleController.getAll);
router.get("/:id", ArticleController.get);
router.post("/", ArticleController.create);
router.get("/:postId/comments", CommentController.getCommentsByPostId);
router.post("/:id/like", ArticleController.likeArticle);

module.exports = router;
