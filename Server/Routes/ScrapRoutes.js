const axios = require("axios");
const cheerio = require("cheerio");
const shortId = require("shortid");
const express = require("express");
const router = express.Router();

router.get("/", (req, res) =>
  getAll(req.query.q).then(e => res.json({ message: "success", body: e }))
);

function getAll(param) {
  shortId.characters(
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@"
  );
  const url = "https://www.ambitionbox.com/overview";

  let data = [];
  const getSinglePage = offset =>
    axios.get(`${url}?page=${offset}`).then(response => {
      var $ = cheerio.load(response.data);

      $(".companies_tiles_wrap")
        .find(".company_tile_wrap")
        .map((id, row) =>
          data.push({
            id: shortId.generate(),
            companyName: $(row)
              .find(".company_name")
              .text(),
            imageUrl: $(row)
              .find("img")
              .attr("src")
          })
        );
      return data;
      /*
      offset++;
      if (offset <= 2) {
        return getSinglePage(offset);
      } else {
        return data;
      } */
    });

  return getSinglePage(param);
}

module.exports = router;
