const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
app.use(cors());
app.use(bodyParser.json());
app.use("/articles", require("./Routes/ArticleRoutes"));
app.use("/scrape", require("./Routes/ScrapRoutes"));
app.use("/comments", require("./Routes/CommentRoutes"));

app.get("/temp", (req, res) => {
  const Article = require("./Models/Article");
  const article = new Article().updateLikes("WahymFCc9");
  res.json({
    data: article
  });
});

app.listen(5000, () => console.log("server running"));
